FROM ubuntu:bionic

ENV TZ=Europe/Berlin
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update --fix-missing \
  && apt-get -y install sudo python-pip wget ssh mc vim curl

RUN sudo pip install rethinkdb==2.4.8

RUN curl -LJO https://github.com/Backblaze/B2_Command_Line_Tool/releases/download/v3.2.0/b2-linux

RUN cp b2-linux /usr/local/bin/b2

RUN chmod +x /usr/local/bin/b2

RUN b2 authorize_account 10549dcb3e78 0014ed7aaa9668faac73eece92ca82bdb74f4e63f3

ADD run.sh /run.sh
ADD rdbpass /rdbpass

VOLUME ["/backups"]

CMD ["/run.sh"]
