DOCKER = docker
REPO = crofflr/rethinkdb-backup
REMOTEREPO = registry.gitlab.com/mzehrer/rethinkdb-backup

TAG = $(shell git rev-parse --abbrev-ref HEAD 2>/dev/null)
ifeq ($(TAG), master)
            TAG = latest
    else ifeq ($(TAG), HEAD)
            TAG = latest
    endif
TAG = latest

all: build

run:
	docker-compose up

shell:
	$(DOCKER) run -i -t $(REPO) /bin/bash

build:
	$(DOCKER) build -t $(REPO):$(TAG) .

push:
	$(DOCKER) tag $(REPO):$(TAG) $(REMOTEREPO):$(TAG)
	$(DOCKER) push $(REMOTEREPO):$(TAG)
