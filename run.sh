#!/bin/bash

BACKUP_NAME=rethinkdb_$(date +"%Y-%m-%d_%H-%M-%S").tar.gz

echo "Creating backup $BACKUP_NAME"
rethinkdb-dump --password-file /rdbpass -c rethinkdb-rethinkdb-proxy -f /backups/$BACKUP_NAME

echo "Uploading backup $BACKUP_NAME to b2"
/usr/local/bin/b2 upload-file --noProgress crofflr-backup /backups/$BACKUP_NAME rdb/$BACKUP_NAME

echo "Updating uptime monitoring dead man switch"
wget https://uptime.crofflr.net/api/push/mkjcBZhS26?msg=OK&ping=1

echo "CleanUp..."
rm /backups/$BACKUP_NAME

echo "Backup $BACKUP_NAME finished"
